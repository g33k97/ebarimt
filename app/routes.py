from flask import request
import json
import os
import shutil
from app import app
from posapi import PosApi


@app.route('/')
def health():
    return "ebarimt service is running healthy!"


@app.route('/checkApi')
def checkApi():
    register = request.args.get("register")
    result = PosApi(register).checkApi()
    return result


@app.route('/getInformation')
def getInformation():
    register = request.args.get("register")
    result = PosApi(register).getInformation()
    return result


@app.route('/callFunction', methods=['POST'])
def callFunction():
    functionName = request.json['functionName']
    str_functionName = json.dumps(functionName)

    data = request.json['data']
    str_data = json.dumps(data)
    register = request.args.get("register")
    result = PosApi(register).callFunction(
        str_functionName.encode('ascii'), str_data.encode('ascii'))

    return result


@app.route('/put', methods=['POST'])
def put():
    data = request.json['data']
    str_data = json.dumps(data)
    register = request.args.get("register")
    result = PosApi(register).put(str_data.encode('ascii'))
    return result


@app.route('/returnBill', methods=['POST'])
def returnBill():
    data = request.json['data']
    str_data = json.dumps(data)
    register = request.args.get("register")
    result = PosApi(register).returnBill(str_data.encode('ascii'))
    return result


@app.route('/sendData')
def sendData():
    register = request.args.get("register")
    result = PosApi(register).sendData()
    return result

@app.route('/upload', methods=['POST'])
def upload_so_file():
    so_file = request.files['file']
    register = request.args.get("register")
    directory_path = os.path.join("../poslib", register)
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)

    so_file.save(os.path.join(directory_path, 'libPosAPI.so'))
    return "true"

@app.route('/remove', methods=['GET'])
def remove_file():
    register = request.args.get("register")
    directory_path = os.path.join("../poslib/{}".format(register))

    if os.path.exists(directory_path) and os.path.isdir(directory_path):
        try:
            shutil.rmtree(directory_path)
            return "true"
        except Exception as e:
            return f"Error while removing directory: {str(e)}"
    else:
        return "true"